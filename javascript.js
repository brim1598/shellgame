let shufflebtn = document.getElementById("shuffleBtn");
let container = document.getElementsByClassName("flexbox")[0];
let trapezoidcontainers = Array.prototype.slice.call(
  container.getElementsByClassName("trapezoidcontainer")
);
let trapezoids = document.getElementsByClassName("trapezoid");
let message = document.getElementById("message");
let userscore = document.getElementById("userscore");
let computerscore = document.getElementById("computerscore");

const shuffleArray = array => {
  for (var i = array.length - 1; i > 0; i--) {
    var j = Math.floor(Math.random() * (i + 1));
    var temp = array[i];
    array[i] = array[j];
    array[j] = temp;
  }
  return array;
};

const shuffle = () => {
  // remove elements
  trapezoidcontainers.forEach(function(element) {
    container.removeChild(element);
  });

  shuffleArray(trapezoidcontainers);

  trapezoidcontainers.forEach(function(element) {
    container.appendChild(element);
  });
};

const addStyleClass = (array, name) => {
  [...array].forEach(element => {
    element.classList.add(name);
  });
};

const removeStyleClass = (array, name) => {
  [...array].forEach(element => {
    element.classList.remove(name);
  });
};

// shows where is the ball
const setAnimation = async (array, name) => {
  addStyleClass(array, name);
  await new Promise(r => setTimeout(r, 4000));
  removeStyleClass(array, name);
};

const checkIsWin = async event => {
  [...trapezoidcontainers].forEach(value => {
    value.onclick = null;
  });
  let str = "";
  if (event.target.nextElementSibling) {
    str = "you win";
    userscore.textContent = parseInt(userscore.textContent) + 1;
  } else {
    str = "you lose";
    computerscore.textContent = parseInt(computerscore.textContent) + 1;
  }
  message.textContent = str;
  await setAnimation(trapezoids, "animation");
};

const start = async () => {
  message.textContent = "which one is it?";

  await setAnimation(trapezoids, "animation");

  // add shuffleanimation
  [...trapezoidcontainers].forEach((value, index) => {
    addStyleClass([value], "shuffleanimation" + (index + 1));
  });
  await new Promise(r => setTimeout(r, 4000));
  [...trapezoidcontainers].forEach((value, index) => {
    removeStyleClass([value], "shuffleanimation" + (index + 1));
  });

  shuffle();

  // add onclick for cups
  [...trapezoidcontainers].forEach(value => {
    value.onclick = checkIsWin;
  });
};

shufflebtn.onclick = start;
